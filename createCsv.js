var request = require('request');
var fs = require('fs');

var url = "https://www.thefreshmarket.com/api/content/recipes";

var startDate = new Date("2011-06-01 00:00:00");
var endDate = new Date("2020-06-30 23:59:59")

var fileWrite = [];

request(url, function (error, response, body) {
    if (error) {
        console.log(error);
    }
    var recipes = JSON.parse(body.replace(/\\n/g,"")).recipes;
    monthRecipes = recipes.filter(recipe => new Date(recipe.recipePostDate) > startDate && new Date(recipe.recipePostDate) < endDate)
    var sample = monthRecipes[0].recipeIngredients.replace(/\\n/g,"");
    monthRecipes.forEach(recipe => {
        fileWrite.push({
            name: recipe.recipeName,
            date: recipe.recipePostDate,
            directions: recipe.directions,
            rating: recipe.aggregateRating,
            largeImage: "https:" + recipe.recipeLargeImage.url,
            thumbImage: "https:" + recipe.recipeThumbnailImage.url,
            ingredients: recipe.recipeIngredients,
            category: recipe.recipeCategory
        })
    });
    fs.writeFile('ingredients.json', JSON.stringify(fileWrite));
});